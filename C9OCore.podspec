Pod::Spec.new do |spec|

  spec.name                     = "C9OCore"
  spec.version                  = "1.0.4"
  spec.summary                  = "C9OCore is provided by Cloud9 Online LLC which includes the generic features related to Cloud9 Online applications."
  spec.description              = "C9OCore provides the all generic things which are needed by Cloud9 Online applications. That's why all those application must installed this library to get the most of the things."
  spec.homepage                 = "https://gitlab.com/c9opublicframeworks/c9ocoreframework"
  spec.license                  = { :type => 'MIT', :text => <<-LICENSE
                                        Copyright © 2021 Cloud9 Online LLC.
                                    LICENSE
                                  }
  spec.author                   = { "Cloud9 Online" => "cloud9online@gmail.com" }
  spec.ios.deployment_target    = "11.0"
  spec.source                   = { :git => "https://gitlab.com/c9opublicframeworks/c9ocoreframework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks      = "C9OCore.framework"
  spec.swift_versions           = ['5.0']

  spec.dependency                 'ShimmerSwift'
  spec.dependency                 'RealmSwift', "~> 5.5.0"
  spec.dependency                 'ReachabilitySwift'
  
  spec.user_target_xcconfig     = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

end
