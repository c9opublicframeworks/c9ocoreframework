# C9OCore

C9OCore is provided by Cloud9 Online LLC which includes the generic features related to Cloud9 Online applications.

## Requirements
- Xcode 12.0 or later
- iOS 11.0 or later
- Swift 5.0 or later

## Installation
### Cocoapods
1. This framework is available through [CocoaPods](http://cocoapods.org). 
2. Include C9OCore in your `Pod File`:

```
pod 'C9OCore'
```

## Usage
### Initialization
Initialization can be done in AppDelegate's application:didFinishLaunchingWithOptions: method or any swift file.

```swift
import C9OCore

C9OCore.shared.configure(
            apiKey: "YOUR API KEY",
            frameworks: [Other_Cloud9_Framework1.self,
                         Other_Cloud9_Framework2.self,
                         Other_Cloud9_Framework3.self
            ],
            iapServiceConfiguration: C9OIAPServiceConfiguration(appSecret: "APPLICATION SECRET FOR IN-APP PURCHASES"),
            databaseManagerConfiguration: [C9ODatabaseConfiguration(name: "REALM NAME", types: [Object.self, Object.self])],
            completion: {(response, error) in 
            }
)
```

- `apiKey` An API key is required to initialize the C9OCore framework.
- `frameworks` You can pass a list of other Cloud9 Online frameworks which will be initialized through C9OCore framework. If you don't want to use any other framework, you can opt-out from this parameter.
- `iapServiceConfiguration` This parameter is used for **C9OIAPService** framework of Cloud9 Online. If you are not using this framework in your project, you can opt-out from this parameter.
- `databaseManagerConfiguration` This parameter is used to initialize a list of **Realms**:
    - `name` Name of Realm which you want to initialize through C9OCore.
    - `types` List of classes which you want to use in this Realm. If you don't to use any database related functionality, you can opt-out from this parameter. Each class must be inherited from **Object** class of Realm SDK. Following classes are already available in this framework:
        - Meditation
        - MeditationCategory
        - Metric
        - User
        - SystemMessage
        - SystemNotification
- `completion` A callback will be called once initialization will be completed which includes the initialization response and error.

# C9ODatabaseManager
## Database
### Get Data    
Use following method to get list of objects.

```swift
import C9ODatabaseManager

C9ODatabaseManager.shared.objects(type: ClassName.self) { (results, error) in
}
```

### Add Data    
Use following method to add data into database.

```swift
import C9ODatabaseManager

C9ODatabaseManager.shared.add(type: ClassName.self, object: ClassName()) { (error) in
}
```

### Delete Data    
Use following method to delete data from database.

```swift
import C9ODatabaseManager

C9ODatabaseManager.shared.delete(type: ClassName.self, object: ClassName()) { (error) in
}
```

### Edit Data    
Use following method to edit data in database.

```swift
import C9ODatabaseManager

C9ODatabaseManager.shared.beginWrite(type: ClassName.self) { (beginError) in
    if let beginError = beginError {
        print(beginError.message)
    } else {

        object.someAttribute = newValue

        C9ODatabaseManager.shared.commitWrite(type: User.self) { (commitError) in                            
            if let commitError = commitError {
                print(commitError.message)
            }                               
        }
    }
}
```

### Sort Meditations by playback metric
Use following method to sort meditations by their playback history.

```swift
import C9ODatabaseManager

C9ODatabaseManager.shared.meditationsSortedByPlayback(meditations: [Meditation]) { (sortedMeditations, error) in
}
```

### Get Recommended Meditations
Use following method to get recommended meditations which are calculcated by listening history of user.

```swift
import C9ODatabaseManager

C9ODatabaseManager.shared.getRecommendedMeditations(userIdentity: Device UUID) { (meditations, error) in
}
```

### Get Trending Meditations
Use following method to get trending meditations which are calculcated by listening history.

```swift
import C9ODatabaseManager

C9ODatabaseManager.shared.getTrendingMeditations { (meditations, error) in
}
```


## Observers
### Subscribe
```swift
let observerClient = C9ODatabaseManager.shared.addObserver(observer: self)
```

After subscribing for observer, you can listen for different states of database by using following delegate functions:
```swift
extension ViewController: C9ODatabaseManagerObserver {
        
    func syncingDidCompleted(for database: C9ODatabaseConfiguration) {
    }
    
    func syncingDidFailed(for database: C9ODatabaseConfiguration, error: C9OError) {
    }
    
    func dataDidUpdated(for type: C9ODatabaseObjectType, in database: C9ODatabaseConfiguration) {
    }
    
    func unableToAttachObserver(for type: C9ODatabaseObjectType, in database: C9ODatabaseConfiguration, error: C9OError) {
    }

}
```

### Unsubscribe
```swift
C9ODatabaseManager.shared.removeObserver(client: observerClient)
```

# C9OCore logging
By default, logging is **disabled** for all Cloud9 Online frameworks. You can enable logging by setting `C9O_LOGS = enable` environment variable in your project scheme.


